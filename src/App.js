import "./App.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Hero from "./components/Hero";
import Survey from "./components/Survey";
import Faq from "./components/FAQ";

function App() {
  return (
    <>
      <Header />
      <Hero />
      <Survey />
      <Faq />
      <Footer />
    </>
  );
}

export default App;

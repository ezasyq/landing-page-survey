import React from "react";
import Done from "../asset/completed.png";

function Completed() {
  return (
    <>
      <h5 className="completed_title">
        Selamat, Anda berhasil berpartisipasi dalam survei ini!
      </h5>
      <p className="completed_desc">Hadiah akan diundi di Instagram @yokesen_id, <br/>
        hari Senin, 29 Maret 2021 pukul 15.00 WIB.</p>
      <div className="completed_img">
        <img src={Done} alt="" />
      </div>
      <p className="completed_text">
      Semoga beruntung!
      </p>
    </>
  );
}

export default Completed;

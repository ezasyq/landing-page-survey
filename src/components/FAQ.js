import React, { useState } from "react";
import "../css/FAQ.css";
import { Accordion, Icon } from "semantic-ui-react";

function Faq() {
  const [activeIndex, setActiveIndex] = useState(0);

  const handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const newIndex = activeIndex === index ? -1 : index;
    setActiveIndex(newIndex);
  };

  return (
    <>
      <div className="container">
        <div className="faq_container" id="FAQ">
          <h5>Pertanyaan Umum</h5>
          <Accordion fluid>
            <Accordion.Title
              active={activeIndex === 0}
              index={0}
              onClick={handleClick}
              className="survey_accordion"
            >
              Apa yang akan saya dapatkan jika berpartisipasi dalam survei?
              <Icon name="chevron down" />
            </Accordion.Title>
            <Accordion.Content
              active={activeIndex === 0}
              className="accordion_item"
            >
              <p>
              Anda akan berpeluang mendapatkan hadiah berupa Haylou GT1 Pro TWS Wireless Earphone Bluetooth 5.0 untuk 5 orang pemenang yang akan di undi Instagram @yokesen_id, 
hari Senin, 29 Maret 2021 pukul 15.00 WIB.
              </p>
            </Accordion.Content>

            <Accordion.Title
              active={activeIndex === 1}
              index={1}
              onClick={handleClick}
              className="survey_accordion"
            >
              Apa saja langkah yang diperlukan untuk bisa berpartisipasi dalam
              survei ini?
              <Icon name="chevron down" />
            </Accordion.Title>
            <Accordion.Content
              active={activeIndex === 1}
              className="accordion_item"
            >
              <p>
                Untuk berpartisipasi, Anda hanya perlu menjawab survei dan
                mengisi data diri.
              </p>
            </Accordion.Content>

            <Accordion.Title
              active={activeIndex === 2}
              index={2}
              onClick={handleClick}
              className="survey_accordion"
            >
              Bagaimana hadiah akan dikirimkan kepada saya?
              <Icon name="chevron down" />
            </Accordion.Title>
            <Accordion.Content
              active={activeIndex === 2}
              className="accordion_item"
            >
              <p>
                Hadiah akan dikirimkan ke alamat yang Anda submit dalam tahap
                pengisian data diri.
              </p>
            </Accordion.Content>

            <Accordion.Title
              active={activeIndex === 3}
              index={3}
              onClick={handleClick}
              className="survey_accordion"
            >
              Apa tujuan dari survei ini?
              <Icon name="chevron down" />
            </Accordion.Title>
            <Accordion.Content
              active={activeIndex === 3}
              className="accordion_item"
            >
              <p>
                Survei ini bertujuan untuk mengetahui preferensi pengguna
                marketplace dalam berbelanja
              </p>
            </Accordion.Content>

            <Accordion.Title
              active={activeIndex === 4}
              index={4}
              onClick={handleClick}
              className="survey_accordion"
            >
              Siapakah yang menyelenggarakan survei ini?
              <Icon name="chevron down" />
            </Accordion.Title>
            <Accordion.Content
              active={activeIndex === 4}
              className="accordion_item"
            >
              <p>
                Survei ini diselenggarakan oleh PT. Yokesen Teknologi Indonesia.
              </p>
            </Accordion.Content>
          </Accordion>
        </div>
      </div>
    </>
  );
}

export default Faq;

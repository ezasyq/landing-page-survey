import React from "react";
import "../css/Footer.css";
import Mail from "../asset/icons/mail.svg";
import FB from "../asset/icons/fb.svg";
import IG from "../asset/icons/ig.svg";
import IN from "../asset/icons/in.svg";

function Footer() {
  return (
    <>
      <div className="footer">
        <div className="footer_left">
          <div>
            <h5>Contact Us</h5>
          </div>
          <div>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="mailto:contact@yokesen.com"
            >
              <img src={Mail} alt="mail" />
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.facebook.com/yokesen.id/"
            >
              <img src={FB} alt="fb" />
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.instagram.com/yokesen_id/?hl=en"
            >
              <img src={IG} alt="ig" />
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://id.linkedin.com/company/yokesentechnology "
            >
              <img src={IN} alt="linkedin" />
            </a>
          </div>
        </div>

        <div className="footer_right">
          <p>Copyright ©Yokesen 2021</p>
        </div>
      </div>
    </>
  );
}

export default Footer;

import React from "react";
import "../css/Header.css";
// import Logo from "../asset/logo.png";

function Header() {
  return (
    <>
      {/* <div className="header">
        <div className="header_logo">
          <img src={Logo} alt="" />
        </div>
        <div className="header_links">
          <ul>
            <li className="links">
              <a href="https://yokesen.com/en/about/yokesen">About</a>
            </li>
            <li className="links">
              <a href="google.com">FAQ</a>
            </li>
            <li className="links">
              <a href="https://yokesen.com/en/contact">Contact</a>
            </li>
          </ul>
        </div>
      </div> */}

<nav className="navbar_agerabot">
    <input id="nav-toggle" type="checkbox" />
    <div className="logo"></div>
    <ul className="links">
        <li>
          <a href="https://yokesen.com/en/about/yokesen" className="nav__link">About</a>
        </li>
        <li>
          <a href="#FAQ" className="nav__link">FAQ</a>
        </li>
        <li>
          <a href="https://yokesen.com/en/contact" className="nav__link">Contact Us</a>
        </li>
    </ul>
    <label htmlFor="nav-toggle" className="icon-burger">
        <div className="line"></div>
        <div className="line"></div>
        <div className="line"></div>
    </label>
</nav>

    </>
  );
}

export default Header;

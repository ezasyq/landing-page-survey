import React from "react";
import "../css/Hero.css";
import Step1 from "../asset/point1.png";
import Step2 from "../asset/point2.png";
import Step3 from "../asset/point3.png";

function Hero() {
  return (
    <>
      <div className="hero-container">
        <div className="container">
          <div className="hero-title">
            <h5>Menangkan 5 buah Earbuds senilai 1 juta rupiah</h5>
            <p>Bantu kami isi survei sekarang dan dapatkan hadiahnya!</p>
            <div className="survey-step">
              <div className="steps">
                <img src={Step1} alt="" />
                <span>Jawab Pertanyaan</span>
              </div>
              <div className="steps">
                <img src={Step2} alt="" />
                <span>Isi Data Diri</span>
              </div>
              <div className="steps">
                <img src={Step3} alt="" />
                <span>Menangkan Hadiahnya</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Hero;

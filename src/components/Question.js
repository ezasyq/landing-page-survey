import React, { useState } from "react";
import { Checkbox } from "semantic-ui-react";
import "../css/Question1.css";

function Quest1(props) {
  const [selected1, setSelected1] = useState(false);
  const [selected2, setSelected2] = useState(false);
  const [selected3, setSelected3] = useState(false);
  const [selected4, setSelected4] = useState(false);

  const handleClick = (x) => {
    switch (x) {
      case 1:
        setSelected1(!selected1);
        break;
      case 2:
        setSelected2(!selected2);
        break;
      case 3:
        setSelected3(!selected3);
        break;
      case 4:
        setSelected4(!selected4);
        break;
      default:
        break;
    }
  };

  const getCheck = () => {
    let result = [
      { checkbox1: selected1 },
      { checkbox2: selected2 },
      { checkbox3: selected3 },
      { checkbox4: selected4 },
    ];
    console.log(result);
  };

  return (
    <>
      <div className="quest_container">
        <p>
          Apa alasanmu mempertimbangkan <strong>lokasi penjual</strong> ketika
          berbelanja?{" "}
        </p>
        <Checkbox
          className={selected1 ? "quest_choice selected" : "quest_choice"}
          label="Ongkos kirim lebih murah"
          onClick={() => handleClick(1)}
        />
        <Checkbox
          className={selected2 ? "quest_choice selected" : "quest_choice"}
          label="Barang lebih cepat sampai"
          onClick={() => handleClick(2)}
        />
        <Checkbox
          className={selected3 ? "quest_choice selected" : "quest_choice"}
          label="Lebih mudah di retur saat ada komplain"
          onClick={() => handleClick(3)}
        />
        <Checkbox
          className={selected4 ? "quest_choice selected" : "quest_choice"}
          label="Kepercayaan terhadap seller"
          onClick={() => handleClick(4)}
        />
        <div className="survey_submit">
          {/* <button onClick={props.handleStep}>Submit</button> */}
          <button onClick={getCheck}>Submit</button>
        </div>
      </div>
    </>
  );
}

export default Quest1;

import React, { useState, useEffect } from "react";
import { Checkbox } from "semantic-ui-react";
import axios from "axios";
import "../css/Survey.css";
import Stepper from "react-stepper-horizontal";
// import CheckIcon from "../asset/icons/check.png";
import Completed from "./Completed";
import { Button, Icon, Form, TextArea, Dropdown } from "semantic-ui-react";

function Survey() {
  // Radio Var
  const [cOne, setcOne] = useState(false);
  const [cTwo, setcTwo] = useState(false);
  const [queOne, setQueOne] = useState({ one: "", two: "" });

  // Checkbox Var
  const [selected1, setSelected1] = useState(false);
  const [selected2, setSelected2] = useState(false);
  const [selected3, setSelected3] = useState(false);
  const [selected4, setSelected4] = useState(false);
  // const [selectRes, setSelectRes] = useState(null);

  // Stepper Var
  const [steps, setSteps] = useState(1);
  const [dataStep, setDataStep] = useState(null);

  // Data Var
  const [address, setAddress] = useState("");
  const [username, setUsername] = useState("");
  const [phone, setPhone] = useState("");
  const [userProv, setUserProv] = useState("");
  const [userKota, setUserKota] = useState("");

  const [inputError, setInputError] = useState(false);
  const [provinsi, setProvinsi] = useState(null);
  const [city, setCity] = useState(null);

  // ====================================

  useEffect(() => {
    axios
      .get("https://dev.farizdotid.com/api/daerahindonesia/provinsi")
      .then((res) => {
        // console.log(res.data.provinsi.length);
        let dataProv = res.data.provinsi;
        let x = [];
        dataProv.forEach((element) => {
          x.push({
            key: element.id,
            text: element.nama,
            value: element.nama,
          });
        });
        setProvinsi(x);
        // console.log(x);

        // setProvinsi([res.data.provinsi])
      })
      .catch((err) => console.log(err));
  }, []);

  // Handle Radio Select
  const handleChoiceOne = () => {
    setcOne(true);
    setcTwo(false);
    setInputError(false);
    return;
  };

  const handleChoiceTwo = () => {
    setcOne(false);
    setcTwo(true);
    setInputError(false);
    return;
  };
  // End

  // Handle Checkbox Select
  const handleClick = (x) => {
    switch (x) {
      case 1:
        setSelected1(!selected1);
        setInputError(false);

        break;
      case 2:
        setSelected2(!selected2);
        setInputError(false);

        break;
      case 3:
        setSelected3(!selected3);
        setInputError(false);

        break;
      case 4:
        setSelected4(!selected4);
        setInputError(false);

        break;
      default:
        break;
    }
  };
  // End

  // Quest One Func
  const handleQuestOne = () => {
    if (cOne === false && cTwo === false) {
      setInputError(true);
      return;
    }

    if (cOne === true) {
      setQueOne({ one: true, two: false });
      setSteps(steps + 1);
    } else if (cTwo === true) {
      setQueOne({ one: false, two: true });
      setSteps(steps + 2);
      setDataStep(0);
    }
  };
  // End

  //Quest Two Func
  const handleQuestTwo = () => {
    if (
      selected1 === false &&
      selected2 === false &&
      selected3 === false &&
      selected4 === false
    ) {
      setInputError(true);
      return;
    }

    setSteps(steps + 1);
    setDataStep(0);
  };

  // Get Input Value
  const handleChange = (input) => (e) => {
    if (input === "address") {
      setAddress(e.target.value);
      setInputError(false);
      // console.log(address);
    } else if (input === "name") {
      setUsername(e.target.value);
      setInputError(false);
      // console.log(username);
    } else if (input === "phone") {
      setPhone(e.target.value);
      setInputError(false);
      // console.log('phone')
    }
  };

  const nextStep = (params) => {
    if (params === "address") {
      if (address === "" || userProv === "" || userKota === "") {
        setInputError(true);
        return;
      }
    } else if (params === "username") {
      if (username === "") {
        setInputError(true);
        return;
      }
    } else if (params === "phone") {
      if (phone === "") {
        setInputError(true);
        return;
      }
    }

    setSteps(steps + 1);
    setDataStep(dataStep + 1);
  };

  const prevStep = () => {
    setSteps(steps - 1);
    setDataStep(dataStep - 1);
  };

  const getProv = (event, { value }) => {
    setUserProv(value);
    let idKota = null;
    provinsi.forEach((element) => {
      if (element.value === value) {
        idKota = element.key;
      }
    });

    if (idKota !== null) {
      axios
        .get(
          `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${idKota}`
        )
        .then((res) => {
          let dataKota = res.data.kota_kabupaten;
          // console.log(dataKota);
          let y = [];
          dataKota.forEach((element) => {
            y.push({
              key: element.id,
              text: element.nama,
              value: element.nama,
            });
          });
          setCity(y);
          setInputError(false);
        })
        .catch((err) => {
          // console.log(err);
        });
    }
  };

  const getKota = (event, { value }) => {
    setUserKota(value);
    setInputError(false);
  };

  const result = () => {
    if (phone === "") {
      setInputError(true);
      return;
    }
    // Get Radio Answer
    let answer1 = "";
    if (queOne.one === true) {
      answer1 = "Mencari seller dengan lokasi terdekat";
    } else if (queOne.two === true) {
      answer1 = "Membeli tanpa mempertimbangkan lokasi seller";
    }

    let res = {
      answer1: answer1,
      alamat: address,
      nama: username,
      noHandphone: phone,
      checkbox1: selected1 === true ? "Ongkos kirim lebih murah" : "",
      checkbox2: selected2 === true ? "Barang lebih cepat sampai" : "",
      checkbox3:
        selected3 === true ? "Lebih mudah di retur saat ada komplain" : "",
      checkbox4: selected4 === true ? "Kepercayaan terhadap seller" : "",
      kota: `${userProv} - ${userKota}`,
    };

    // console.log(res);

    axios
      .post("https://yokesen.com/api/submitSurvey", res)
      .then((response) => {
        // console.log(response);
        nextStep();
      })
      .catch((err) => {
        // console.log(err);
      });
  };

  const renderSurvey = () => {
    switch (steps) {
      case 1:
        return (
          <div>
            <p>
              Manakah yang lebih mendekati <strong>kebiasaan</strong> kamu
              ketika
              <strong> berbelanja</strong> secara online?
            </p>
            <div className="survey_choice">
              <button
                className={cOne === true ? "checked" : ""}
                onClick={handleChoiceOne}
              >
                Mencari seller dengan lokasi terdekat
              </button>
              <button
                className={cTwo === true ? "checked" : ""}
                onClick={handleChoiceTwo}
              >
                Membeli tanpa mempertimbangkan lokasi seller
              </button>
            </div>
            {inputError === true ? (
              <p className="input-error">
                *Pilih salah satu jawaban terlebih dahulu untuk submit
              </p>
            ) : null}
            <div className="survey_submit">
              <button onClick={handleQuestOne}>Submit</button>
            </div>
          </div>
        );
      case 2:
        return (
          <>
            <div className="quest_container">
              <p>
                Apa alasanmu mempertimbangkan <strong>lokasi penjual</strong>{" "}
                ketika berbelanja?{" "}
              </p>
              <Checkbox
                className={selected1 ? "quest_choice selected" : "quest_choice"}
                label="Ongkos kirim lebih murah"
                onClick={() => handleClick(1)}
              />
              <Checkbox
                className={selected2 ? "quest_choice selected" : "quest_choice"}
                label="Barang lebih cepat sampai"
                onClick={() => handleClick(2)}
              />
              <Checkbox
                className={selected3 ? "quest_choice selected" : "quest_choice"}
                label="Lebih mudah di retur saat ada komplain"
                onClick={() => handleClick(3)}
              />
              <Checkbox
                className={selected4 ? "quest_choice selected" : "quest_choice"}
                label="Kepercayaan terhadap seller"
                onClick={() => handleClick(4)}
              />
              {inputError === true ? (
                <p className="input-error">
                  *Pilih salah satu jawaban terlebih dahulu untuk submit
                </p>
              ) : null}
              <div className="survey_submit">
                {/* <button onClick={props.handleStep}>Submit</button> */}
                <button onClick={handleQuestTwo}>Submit</button>
              </div>
            </div>
          </>
        );
      case 3:
        return (
          <div>
            {/* Question Alamat */}
            <div className="button_nav">
              <Button disabled circular icon className="button_left">
                <Icon name="chevron left" />
              </Button>
              <Button
                circular
                icon
                className="button_right"
                onClick={() => nextStep("address")}
              >
                <Icon name="chevron right" />
              </Button>
            </div>
            <p>
              Selanjutnya, jika kamu beruntung, kemanakah hadiah ini akan
              dikirimkan?
            </p>
            <div className="user_input">
              {/* <textarea placeholder="Alamat Lengkap Kamu" name="address" rows="5"></textarea>
              <br/>
              <input
                type="text"
                placeholder="Alamat Kamu"
                onChange={handleChange("address")}
                value={address || ""}
              /> */}
              <Form className="alamat_input">
                <TextArea
                  placeholder="Alamat Kamu"
                  value={address || ""}
                  rows={5}
                  onChange={handleChange("address")}
                />
              </Form>
              <div className="alamat_input">
                <Dropdown
                  placeholder="Pilih Provinsi"
                  fluid
                  selection
                  options={provinsi}
                  onChange={getProv}
                  className="address_dropdown"
                  value={userProv || ""}
                />

                <Dropdown
                  placeholder="Pilih Kota"
                  fluid
                  selection
                  options={city}
                  onChange={getKota}
                  className="address_dropdown"
                  value={userKota || ""}
                />
              </div>
            </div>
            {inputError === true ? (
              <p className="input-error">
                *Masukkan data kamu terlebih dahulu untuk submit
              </p>
            ) : null}
            <div className="survey_submit">
              <button onClick={() => nextStep("address")}>Submit</button>
            </div>
            {/* End Question Alamat */}
          </div>
        );
      case 4:
        return (
          <div>
            <div className="button_nav">
              <Button circular icon className="button_left" onClick={prevStep}>
                <Icon name="chevron left" />
              </Button>
              <Button
                circular
                icon
                className="button_right"
                onClick={() => nextStep("username")}
              >
                <Icon name="chevron right" />
              </Button>
            </div>
            {/* Question Nama */}
            <p>Hadiahnya akan dikirimkan atas nama siapa?</p>
            <div className="user_input">
              <input
                type="text"
                placeholder="Nama Kamu"
                onChange={handleChange("name")}
                value={username || ""}
              />
            </div>
            {inputError === true ? (
              <p className="input-error">
                *Masukkan data kamu terlebih dahulu untuk submit
              </p>
            ) : null}
            <div className="survey_submit">
              <button onClick={() => nextStep("username")}>Submit</button>
            </div>
            {/* End Question Nama */}
          </div>
        );
      case 5:
        return (
          <div>
            <div className="button_nav">
              <Button circular icon className="button_left" onClick={prevStep}>
                <Icon name="chevron left" />
              </Button>
              <Button disabled circular icon className="button_right">
                <Icon name="chevron right" />
              </Button>
            </div>
            {/* Question Phone */}
            <p>Masukkan nomor WhatsApp mu untuk kami bisa hubungi</p>
            <div className="user_input">
              <input
                type="number"
                placeholder="Nomor WhatsApp untuk menghubungi kamu"
                onChange={handleChange("phone")}
                value={phone || ""}
              />
            </div>
            {inputError === true ? (
              <p className="input-error">
                *Masukkan data kamu terlebih dahulu untuk submit
              </p>
            ) : null}
            <div className="survey_submit">
              <button onClick={result}>Submit</button>
            </div>
            {/* End Question Phone */}
          </div>
        );
      case 6:
        return <Completed />;

      default:
        break;
    }
  };

  return (
    <>
      <div className="container">
        {/* Hide Stepper */}
        {dataStep === null ? null : dataStep > 2 ? null : (
          <Stepper
            steps={[
              { title: "Alamat", icon: "check.png" },
              { title: "Nama", icon: "check.png" },
              { title: "Kontak", icon: "check.png" },
            ]}
            activeStep={dataStep}
            activeColor="#FAB207"
            completeColor="#FAB207"
            defaultBarColor="#FAB207"
            completeBarColor="#FAB207"
            defaultColor="#fff"
            defaultBorderStyle="solid"
            defaultBorderColor="#FAB207"
            defaultBorderWidth={2}
            circleFontSize={12}
          />
        )}

        <div className="survey_container">{renderSurvey()}</div>
      </div>
    </>
  );
}

export default Survey;
